# io-key simulator

The container image for the software io-key is
```
registry.gitlab.com/autosen_gmbh/apollo/io-key-simulator:latest
```

Use the following envioronment variables to set the endpoint and 
* `SOFTWAREIOKEY_DEVICE__Address` Hostname (FQDN) or IP of your apollo instance
* `SOFTWAREIOKEY_DEVICE__Port` Port on your apollo to connect to
  *  default *1883* with no encryption
  *  use *8883* for MQTT + TLS encrypted connection. see  [Add MQTT TLS Support for io-key connections](../Readme.md#add-mqtt-tls-support-for-io-key-connections) 
* `SOFTWAREIOKEY_DEVICE__CLIENTID` IMEI of the simulator
  *  default *123456789012345*
  *  this value musst be used in the provisioning call. See  [Device provisioning](../Readme.md#device-provisioning)
 
## Example
[docker-compose.yml](docker-compose.yml)